import java.io.IOException;

import module.Bypass;
import module.CheckOS;
import userinterface.TheMainGui;

public class MainGui {

	public static void main(String[] args) throws IOException, InterruptedException {
		if(!(args.length == 0)){// Check if arg
				if (CheckOS.isWindows()){// CheckOS
					if (args[0].equals("-bypass")){
						Bypass.TheBypass(args[1]);
				    	System.exit(0);
					} 
					if (args[0].equals("-grafikfix")){
						ProcessBuilder process = new ProcessBuilder("java", "-jar", "-Dsun.java2d.d3d=false", module.Variablen.jarpathstring);
						process.start();
						System.out.println("grafikfixed");
						System.exit(0);
					} 
					
				} // END CheckOS
		    } // END Check if arg

			String obfuscate = module.krypton.obfuscate("test ���#+*'_:;,.-!");
			System.out.println(obfuscate + " - " + module.krypton.unobfuscate(obfuscate));
		//GUI
		TheMainGui.openthegui();
		
		//Update
		try {
			module.update.Update.Updater(module.Variablen.MyVersion);
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
		/**Runs every 4 min
		new java.util.Timer().schedule( 
		        new java.util.TimerTask() {
		            @Override
		            public void run() {
						// CODE HERE
		            }
		        }, 
		        60000 * 4
		);
		 */

	}
	
}
