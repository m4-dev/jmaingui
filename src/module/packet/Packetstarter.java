package module.packet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.swing.JOptionPane;

import module.CheckOS;

public class Packetstarter {
	

	public static void AntiAFK(){
		if (CheckOS.isWindows()){

		Start(module.Variablen.puttylink, module.Variablen.pfadwin, false);
		
		}else{
			JOptionPane.showMessageDialog(null,"Windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public static void fourKDownloader(){
		if (CheckOS.isWindows()){
			
			Start(module.Variablen.fourkdownloaderlink, module.Variablen.pfadwin, false);
		
		}else{
			JOptionPane.showMessageDialog(null,"Windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public static void systemconsole(){
		if (CheckOS.isWindows()){
				
			Start(module.Variablen.systemconsolelink, module.Variablen.pfadwin, true);
		
		}else{
			JOptionPane.showMessageDialog(null,"Windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	public static void contexteingabeEin(){
		if (CheckOS.isWindows()){
			try {
			File file = new File(module.Variablen.pfadwin + "temp\\EingabeaufforderungEin.reg");
			if (!file.exists()) {
				new File(module.Variablen.pfadwin + "temp").mkdirs();
			    InputStream link;
				link = (Object.class.getResource("/resource/registry/EingabeaufforderungEin.reg").openStream());
				Files.copy(link, file.getAbsoluteFile().toPath());
			}
			Startwithpath(module.Variablen.pfadwin + "EingabeaufforderungEin.reg",true,true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}else{
			JOptionPane.showMessageDialog(null,"Windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static void contexteingabeAus() {
		if (CheckOS.isWindows()){
			try {
			File file = new File(module.Variablen.pfadwin + "temp\\EingabeaufforderungAus.reg");
			if (!file.exists()) {
				new File(module.Variablen.pfadwin + "temp").mkdirs();
			    InputStream link;
				link = (Object.class.getResource("/resource/registry/EingabeaufforderungAus.reg").openStream());
				Files.copy(link, file.getAbsoluteFile().toPath());
			}
			Startwithpath(module.Variablen.pfadwin + "EingabeaufforderungAus.reg",true,true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}else{
			JOptionPane.showMessageDialog(null,"Windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	
	
	//start with URL
	public static void Start(String link, String pfad, boolean elevate){
		String filepath = Packetconverter.link2filepath(link, pfad).replace(".zip", ".exe");
		try {
		// Windows ===========================================================================================
		if (CheckOS.isWindows()){
			if (elevate == true){
				if (!module.Variablen.elevatefile.exists()) {
					new File(module.Variablen.pfadwin + "temp").mkdirs();
				    InputStream elevatefilestream = (Object.class.getResource("/resource/file/Elevate.exe").openStream());
					Files.copy(elevatefilestream, module.Variablen.elevatefile.getAbsoluteFile().toPath());
				}
				ProcessBuilder process = new ProcessBuilder(module.Variablen.elevatefilestring, filepath);
				Process processToExecute = process.start();
				processToExecute.waitFor();
			}else{
				ProcessBuilder process = new ProcessBuilder(filepath);
				process.start();
			}
		// Linux ===========================================================================================
		}else if (CheckOS.isLinux()){
			
		// Mac ===========================================================================================
		}else if (CheckOS.isMac()){
			
		}
		} catch (IOException | InterruptedException e) {
			JOptionPane.showMessageDialog(null,"Dateien fehlerhaft bitte neu runterladen !" + e,"Error :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	// Start with path
	public static void Startwithpath(String filepath, boolean elevate, boolean reg){
		try {
		// Windows ===========================================================================================
		if (CheckOS.isWindows()){
			if (elevate == true){
				if (!module.Variablen.elevatefile.exists()) {
					new File(module.Variablen.pfadwin + "temp").mkdirs();
				    InputStream elevatefilestream = (Object.class.getResource("/resource/file/Elevate.exe").openStream());
				    Files.copy(elevatefilestream, module.Variablen.elevatefile.getAbsoluteFile().toPath());
				}
				if (reg){
					ProcessBuilder process = new ProcessBuilder(module.Variablen.elevatefilestring,"regedit.exe", "/s", filepath);
					Process processToExecute = process.start();
					processToExecute.waitFor();
				}else{
					ProcessBuilder process = new ProcessBuilder(module.Variablen.elevatefilestring, filepath);
					Process processToExecute = process.start();
					processToExecute.waitFor();
				}
			}else{
				ProcessBuilder process = new ProcessBuilder(filepath);
				process.start();
			}
				
		// Linux ===========================================================================================
		}else if (CheckOS.isLinux()){
			
		// Mac ===========================================================================================
		}else if (CheckOS.isMac()){
			
		}
		} catch (IOException | InterruptedException e) {
			JOptionPane.showMessageDialog(null,"Dateien fehlerhaft bitte neu runterladen !" + e,"Error :(", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}

