package module.packet;

import java.io.File;

public class Packetconverter {

	public static String link2filepath(String link, String pfad){
			
			// Datei name in der URL
		    String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
		    
		    // file ordner name
		    String foldername = urlfilename.substring(0, urlfilename.lastIndexOf("."));
			
			// Der Pfad wo die datei genau gespeichert wird
			String filepath = pfad + foldername + "/" + urlfilename;
			
			return filepath;
	}
	public static String link2folderpath(String link, String pfad){
		
		// Datei name in der URL
	    String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
	    
	    // file ordner name
	    String foldername = urlfilename.substring(0, urlfilename.lastIndexOf("."));
	    
		// Wohin die datei extrahiert werden soll(das ist auch der Speicher Ordner indem die datei/ dateien gespeichert werden) 
		String extractpfad = pfad + foldername;
		
		return extractpfad;
}
	public static boolean link2existfile(String link, String pfad){

		// Datei name in der URL
	    String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
	    
	    // file ordner name
	    String foldername = urlfilename.substring(0, urlfilename.lastIndexOf("."));
	    
		// Wohin die datei extrahiert werden soll(das ist auch der Speicher Ordner indem die datei/ dateien gespeichert werden) 
		String extractpfad = pfad + foldername;
		
		File installed = new File(extractpfad);
		
		return installed.exists();
}
	public static boolean existfile(String Filepath){
		
		File installed = new File(Filepath);
		
		return installed.exists();
}	
	
	
	
}