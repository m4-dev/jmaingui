package module;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class Download {

	public static void download(){
		JOptionPane.showMessageDialog(null,"KEINE DOWNLOAD DATEN ANGEGEBEN","Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void download(String link, String pfad, boolean update){ // hier werden die Downloadparameter angegeben und an den eigentlichen download gegeben
		try {
			// erstelle den main ordner
			createmainfolder();
			
			// Datei name in der URL
		    String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
		    
		    // file ordner name
		    String foldername = urlfilename.substring(0, urlfilename.lastIndexOf("."));
		    
			// Wohin die datei extrahiert werden soll(das ist auch der Speicher Ordner indem die datei/ dateien gespeichert werden) 
			String extractpfad = pfad + foldername;
			
			// Der Pfad wo die datei genau gespeichert wird
			String filepath = pfad + foldername + "/" + urlfilename;
	
		
		if(new File(extractpfad).exists() == false){
		    // Neuer Ordner mit Dateinamen als ordner name
			new File(pfad + foldername).mkdirs();
			// String zu URL
			URL url = new URL(link);
			// starte Download
	    	saveFile(url, filepath, urlfilename, extractpfad, update, module.Variablen.jarpath);
			} else {
			System.out.println("Error Datei Existiert bereits bei fehlern l�schen und neu downloaden" );
	        JOptionPane.showMessageDialog(null,"Datei Existiert bereits bei Fehlern der Datei l�schen und Datei neu downloaden",
	        		"Error", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (IOException e) {
			System.out.println("Error " + e);
	        JOptionPane.showMessageDialog(null,e,"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void checkzipfile(String urlfilename, String filepath, String extractpfad){
    	if(urlfilename.contains(".zip") == true) {
    		Ziptools.extracttoFolder(filepath, extractpfad);
    		Filetools.deletefile(filepath);
    	} else {
    		
    	}
		
	}
	
	public static void startafterdownload(){
		
	}
	
	public static void saveFile(URL url, String filepath,String urlfilename, String extractpfad, boolean update, File jarpath) {// hier wird die datei/Dateien runtergeladen
		 Runnable downloadthread = new Runnable() {
	            public void run() {
	                try {
	                	
	                	
	                	// GUI
	        	        final JProgressBar jProgressBar = new JProgressBar();
	        	        jProgressBar.setMaximum(100000);
	        	        JFrame frame = new JFrame();
	        	        frame.setContentPane(jProgressBar);
	        	        frame.setDefaultCloseOperation(0);
	        	        frame.setSize(400, 80);
	        	        frame.setTitle("Downloading " + urlfilename + "...");
	        	        frame.isActive();
	    		        File optionsFile = new File(Saveuserwindowpos.fileName);
	    		        if (optionsFile.exists()) {
	    		            try {
	    		                Saveuserwindowpos.restoreOptions(frame);
	    		            } catch(IOException ioe) {
	    		                ioe.printStackTrace();
	    		            }
	    		        } else {
	    		        	frame.setLocationByPlatform(true);
	    		        }
	        	        if (update == false){
	        	        	frame.setVisible(true);
	        	        }
	        	        
	        	        // HTTP Verbindung
	                    HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
	                    // Datei gr��e
	                    long completeFileSize = httpConnection.getContentLength();
	                    // Bytes Zwichenspeicher
	                    java.io.BufferedInputStream in = new java.io.BufferedInputStream(httpConnection.getInputStream());
	                    // ort wo die datei geschrieben wird
	                    java.io.FileOutputStream fos = new java.io.FileOutputStream(filepath);
	                    java.io.BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
	                    byte[] data = new byte[1024];
	                    long downloadedFileSize = 0;
	                    int x = 0;
	                    while ((x = in.read(data, 0, 1024)) >= 0) {
	                        downloadedFileSize += x;

	                        // calculate progress
	                        final int currentProgress = (int) ((((double)downloadedFileSize) / ((double)completeFileSize)) * 100000d);

	                        // update progress bar
	                        SwingUtilities.invokeLater(new Runnable() {

	                            @Override
	                            public void run() {
	                            	jProgressBar.setValue(currentProgress);
	                            	
	                            }
	                        });

	                        bout.write(data, 0, x);
	                    }
	                    bout.close();
	                    in.close();
	                    System.out.println("Daten wurden erfolgreich Runtergeladen");

	                    // Pr�ft ob es eine zip datei ist und entpackt sie 
	                    frame.setTitle("Extracting " + urlfilename + "...");
	                    checkzipfile(urlfilename,filepath,extractpfad);
	                    
	                    frame.setTitle("DownloadManager By M4rcellxD Finished!");
	                    frame.setVisible(false);
	                    frame.dispose();
	                } catch (FileNotFoundException e) {
	                } catch (IOException e) {
	            		System.out.println("Error " + e);
	                    JOptionPane.showMessageDialog(null,e,"Error", JOptionPane.ERROR_MESSAGE);
	                }
	            }
	        };
	        try {
		        new Thread(downloadthread).
		        start();
				new Thread(downloadthread).
				join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	
	
	private static void createmainfolder(){
	//create Mainfolder First
	if (CheckOS.isWindows()){
		File file = new File(module.Variablen.pfadwin);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
    	        JOptionPane.showMessageDialog(null,"Ordner Konnte nicht erstellt werden (Nicht genug rechte ?)",
    	        		"Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	
	if (CheckOS.isLinux()){
		File file = new File(module.Variablen.pfadnux);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
                JOptionPane.showMessageDialog(null,"Ordner Konnte nicht erstellt werden (Nicht genug rechte ?)",
                		"Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	
	if (CheckOS.isMac()){
		File file = new File(module.Variablen.pfadmac);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
                JOptionPane.showMessageDialog(null,"Ordner Konnte nicht erstellt werden (Nicht genug rechte ?)",
                		"Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
}

}
