package module.update;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import module.CheckOS;
import module.Filetools;

public class Downloadupdate {

	public static void downloadupdate(){
		JOptionPane.showMessageDialog(null,"KEINE DOWNLOAD DATEN ANGEGEBEN","Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void downloadupdate(String link, String pfad, boolean updater){ // hier werden die Downloadparameter angegeben und an den eigentlichen download gegeben
		try {
			// erstelle den main ordner
			createmainfolder();
			
			// Datei name in der URL
		    String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
		    
		    // file ordner name
		    String foldername = "temp";
		    
			// Wohin die datei extrahiert werden soll(das ist auch der Speicher Ordner indem die datei/ dateien gespeichert werden) 
			String extractpfad = pfad + foldername;
			
			// Der Pfad wo die datei genau gespeichert wird
			String filepath = pfad + foldername + "/" + urlfilename;
			
		    // Neuer Ordner mit Dateinamen als ordner name
			if (new File(pfad + foldername).exists() == true){
				Filetools.deletedir(new File(pfad + foldername));
			}
			
			new File(pfad + foldername).mkdirs();
			// String zu URL
			URL url = new URL(link);
			// starte Download
	    	saveFile(url, filepath, urlfilename, extractpfad, module.Variablen.jarpath,updater);
		} catch (IOException e) {
			System.out.println("Error " + e);
	        JOptionPane.showMessageDialog(null,e,"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void saveFile(URL url, String filepath,String urlfilename, String extractpfad, File jarpath, boolean Updater) {// hier wird die datei/Dateien runtergeladen
		 Runnable downloadthread = new Runnable() {
	            public void run() {
	                try {
	                	

	                	// GUI
	        	        final JProgressBar jProgressBar = new JProgressBar();
	        	        jProgressBar.setMaximum(100000);
	        	        JFrame frame = new JFrame();
	        	        frame.setContentPane(jProgressBar);
	        	        frame.setDefaultCloseOperation(0);
	        	        frame.setSize(400, 80);
	        	        frame.setTitle("Downloading " + urlfilename + "...");
	        	        frame.isActive();
	        	        frame.setResizable(false);
	        			frame.setLocationRelativeTo(null);
	        	        if(!Updater){
	        	        	frame.setVisible(true);
	        	        }
	        	        
	        	        
	        	        // HTTP Verbindung
	                    HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
	                    // Datei gr��e
	                    long completeFileSize = httpConnection.getContentLength();
	                    // Bytes Zwichenspeicher
	                    java.io.BufferedInputStream in = new java.io.BufferedInputStream(httpConnection.getInputStream());
	                    // ort wo die datei geschrieben wird
	                    java.io.FileOutputStream fos = new java.io.FileOutputStream(filepath);
	                    java.io.BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
	                    byte[] data = new byte[1024];
	                    long downloadedFileSize = 0;
	                    int x = 0;
	                    while ((x = in.read(data, 0, 1024)) >= 0) {
	                        downloadedFileSize += x;

	                        // calculate progress
	                        final int currentProgress = (int) ((((double)downloadedFileSize) / ((double)completeFileSize)) * 100000d);

	                        // update progress bar
	                        SwingUtilities.invokeLater(new Runnable() {

	                            @Override
	                            public void run() {
	                            	jProgressBar.setValue(currentProgress);
	                            	
	                            }
	                        });

	                        bout.write(data, 0, x);
	                    }
	                    bout.close();
	                    in.close();
	                    System.out.println("Daten wurden erfolgreich Runtergeladen");
	                    // Pr�ft ob gerrade ein update l�uft
	            			Unpack200.unpack(filepath,extractpfad,urlfilename.replace(".pack.gz", ".jar"));
	            			//module.Filetools.copyFile(extractpfad + "\\update.jar ", jarpath.toString(), true);
	            			//Runtime.getRuntime().exec("cmd /c ping 0.0.0.0 -n 2 > nul & move /y " + extractpfad + "\\update.jar " + jarpath + " & java -jar " + jarpath);
	            			String upder = extractpfad + "\\updater.jar";
	            			String upder2 = '"' + extractpfad + "\\updater.jar" + '"';
	            			String upd = extractpfad + "\\update.jar";
	            			String upd2 = '"' + extractpfad + "\\update.jar" + '"';
	            			String replacepath = jarpath.toString();
	            			String replacepath2 = '"' + jarpath.toString() + '"';
	            			
	            			if (!Updater){
	            				try{
	            					Runtime.getRuntime().exec("java -jar " + upder2 + " " + upd2 + " " + replacepath2);
	            				}catch(Exception e){
	            					Runtime.getRuntime().exec("java -jar " + upder + " " + upd + " " + replacepath);
	            				}
	            				System.exit(0);
	            			}
	                    frame.setTitle("Update Finished!");
	                    frame.setVisible(false);
	                    frame.dispose();
	                } catch (FileNotFoundException e) {
	                } catch (IOException e) {
	            		System.out.println("Error " + e);
	                    JOptionPane.showMessageDialog(null,e,"Error", JOptionPane.ERROR_MESSAGE);
	                }
	            }
	        };
	        try {
		        new Thread(downloadthread).
		        start();
				new Thread(downloadthread).
				join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	
	
	private static void createmainfolder(){
	//create Mainfolder First
	if (CheckOS.isWindows()){
		File file = new File(module.Variablen.pfadwin);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
    	        JOptionPane.showMessageDialog(null,"Ordner Konnte nicht erstellt werden (Nicht genug rechte ?)",
    	        		"Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	
	if (CheckOS.isLinux()){
		File file = new File(module.Variablen.pfadnux);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
                JOptionPane.showMessageDialog(null,"Ordner Konnte nicht erstellt werden (Nicht genug rechte ?)",
                		"Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	
	if (CheckOS.isMac()){
		
	}
}

}
