package module.update;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import module.CheckOS;
import module.Filetools;


public class Update {
			
	public static void Updater(double myversion) throws IOException, InterruptedException{
		 Runnable updatethread = new Runnable() {
	            public void run() {
		String updatelink = checkfornewversion(myversion);
		String updaterlink = getupdater(myversion);
		
		// Check for Updates ===========================================================================================
		if (updatelink != null){ 
			System.out.println("neue version is da Downloade Updater... ");
			
			// windows ===========================================================================================
			if (CheckOS.isWindows()){
			Downloadupdate.downloadupdate(updaterlink, module.Variablen.pfadwin,true);
			Downloadupdate.downloadupdate(updatelink, module.Variablen.pfadwin,false);
			System.out.println("Updater Ready");
			
			
			// Linux ===========================================================================================
			}else if (CheckOS.isLinux()){
			Downloadupdate.downloadupdate(updaterlink, module.Variablen.pfadnux,true);
			Downloadupdate.downloadupdate(updatelink, module.Variablen.pfadnux,false);
			System.out.println("Updater Ready");
			
			
			}else{
			JOptionPane.showMessageDialog(null,"Linux & windows Only!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
			}
			// Kein Update ===========================================================================================
		}else {
			System.out.println("Kein Update");
			Filetools.deletedirsilent(new File(module.Variablen.mainpathmultisystem + "temp"));
			}	
	      }
	    };   
		new Thread(updatethread).
		    start();
	}
	
	public static String checkfornewversion(double myversion){
		double version = Double.parseDouble(checkver().split("_")[0]);
		String updateurl = null;
		if (version > myversion){
			updateurl = checkver().split("_")[1];
		}
		return updateurl;
	}
	public static String getupdater(double myversion){
		double version = Double.parseDouble(checkver().split("_")[0]);
		String updaterurl = null;
		if (version > myversion){
			updaterurl = checkver().split("_")[2];
		}
		return updaterurl;
	}
   
	public static String checkver(){
		String Version = null;
		String url = null;
		String urlupdater = null;
	       try {
	    	   DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    	   DocumentBuilder db = dbf.newDocumentBuilder();
	    	   Document doc = db.parse(new URL(module.Variablen.latestverurl).openStream());
	    		//optional, but recommended
	    		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	    		doc.getDocumentElement().normalize();
	    		NodeList nList = doc.getElementsByTagName("update");
	    		for (int temp = 0; temp < nList.getLength(); temp++) {
	    			Node nNode = nList.item(temp);
	    			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	    				Element eElement = (Element) nNode;
	    				Version = eElement.getElementsByTagName("version").item(0).getTextContent();
	    				url = eElement.getElementsByTagName("url").item(0).getTextContent();
	    				urlupdater = eElement.getElementsByTagName("urlupdater").item(0).getTextContent();
	    			}
	    		}
	    	    } catch (Exception e) {
	    		e.printStackTrace();
	    	    }
		return Version + "_" + url + "_" + urlupdater;
	}
	
	
	
    }

