package module.update;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;
import java.util.jar.Pack200.Unpacker;
import java.util.zip.GZIPInputStream;

import module.Filetools;


public class Unpack200 {
	
	public static void unpack(String packgz,String extractpath, String extractdname) throws IOException{
	    try {
	    	String packfile = packgz.replace(".gz", "");
	    	String outputjar = extractpath + "\\" + extractdname;
	    	
	        decompressGzipFile(packgz,packfile);
	        File f = new File(packfile);
	        FileOutputStream fostream = new FileOutputStream(outputjar);
	        JarOutputStream jostream = new JarOutputStream(fostream);
	        
	        Unpacker unpacker = Pack200.newUnpacker();
	        // Call the unpacker
	        unpacker.unpack(f, jostream);
	        // Must explicitly close the output.
	        jostream.close();
	        Filetools.deletefile(packgz);
	        Filetools.deletefile(packfile);
	    } catch (IOException ioe) {
	        ioe.printStackTrace();
	    }
	}
	
	 private static void decompressGzipFile(String gzipFile, String newFile) {
	        try {
	            FileInputStream fis = new FileInputStream(gzipFile);
	            GZIPInputStream gis = new GZIPInputStream(fis);
	            FileOutputStream fos = new FileOutputStream(newFile);
	            byte[] buffer = new byte[1024];
	            int len;
	            while((len = gis.read(buffer)) != -1){
	                fos.write(buffer, 0, len);
	            }
	            //close resources
	            fos.close();
	            gis.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	    }
}
