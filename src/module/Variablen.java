package module;

import java.io.File;

public class Variablen {
	
	/** PATH'S */
	// MAINPATH
	public final static String pfadwin = System.getenv("APPDATA") + "\\MainGUI\\";
	public final static String pfadnux = System.getProperty("user.home") + "//MainGUI//";
	public final static String pfadmac = System.getProperty("user.home") + "//MainGUI//";
	// MAINPATH multisystem
	public final static String mainpathmultisystem = mmsp();
	public static String mmsp(){
		String thepath = null;
		// Windows ===========================================================================================
    	if (CheckOS.isWindows()){
    		thepath = System.getenv("APPDATA") + "\\MainGUI\\";
		// Linux ===========================================================================================
		}else if (CheckOS.isLinux()){
			thepath = System.getProperty("user.home") + "//MainGUI//";
		// Mac ===========================================================================================
		}else if (CheckOS.isMac()){
			thepath = System.getProperty("user.home") + "//MainGUI//";
		}
		return thepath;
	 }
	// JAR PATH
	public final static File f = new File(System.getProperty("java.class.path"));
	public final static File jarpath = f.getAbsoluteFile();
	public final static String jarpathstring = f.getAbsoluteFile().toString();
	/** ENDE PATH'S */
	
	/** MAINGUI */
	final public static double MyVersion = 9.1;
	final public static String savewindowposfile = "options.prop";
	// MAINGUI LINKS
	final public static String myurl = "ProjectURL";
	final public static String mainguiurl = "ProjectURL/";
	final public static String latestverurl = "ProjectURL/latest.xml";
	// SETTINGS INI 
	final public static String settingsinipath = mainpathmultisystem + "settings.ini";
	
	// ELEVATE
	final public static String elevatefilestring = pfadwin + "temp\\Elevate.exe";
	final public static File elevatefile = new File(pfadwin + "temp\\Elevate.exe");

	/** ENDE MAINGUI */
	
	/** INTERNET RADIO SENDER */
	public final static String technobase = "http://listen.technobase.fm/tunein-mp3-pls";
	public final static String iloveradio = "http://stream01.iloveradio.de/iloveradio1.mp3";
	public final static String iloveradiobass = "http://stream01.iloveradio.de/iloveradio4.mp3";
	public final static String fantasydancefm = "http://62.138.154.16:8000/HQ_stream.mp3";
	public final static String bigcitybeats = "http://de-hz-fal-stream02.rautemusik.fm/bigcitybeats";
	/** ENDE INTERNET RADIO SENDER */

	/** PACKET LINKS */
	public final static String puttylink 			= "ProjectURL/aafk/AntiAFK.exe";
	public final static String fourkdownloaderlink 	= "ProjectURL/4kvideodownloader.zip";
	public final static String systemconsolelink	= "ProjectURL/sc/SystemConsole.exe";
	/** ENDE PACKET LINKS */

}
