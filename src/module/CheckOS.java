package module;

import java.util.Locale;

public final class CheckOS
	    {
	       private static String OS = null;
	    
	       public static String getOsName()
	       {
	          if(OS == null) { OS = System.getProperty("os.name").toLowerCase(Locale.ENGLISH); }
	          return OS;
	       }
	       
	       public static boolean isWindows()
	       {
	          return getOsName().contains("windows");
	       }
	       public static boolean isLinux()
	       {
		          return getOsName().contains("linux");
		   }
	       public static boolean isMac()
	       {
		          return getOsName().contains("mac");
		   }
	    }