package module;

import java.io.File;

import javax.swing.JOptionPane;

public class Delete {

	public static void deletedir(){
		JOptionPane.showMessageDialog(null,"KEINE DATEN ANGEGEBEN","Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void deletedir(String link, String pfad){ // hier werden die Downloadparameter angegeben und an den eigentlichen download gegeben
		// Datei name in der URL
		String urlfilename = link.substring(link.lastIndexOf('/') + 1, link.length());
		
		// file ordner name
		String foldername = urlfilename.substring(0, urlfilename.lastIndexOf("."));
		
		// Wohin die datei extrahiert werden soll (das ist auch der Speicher Ordner indem die datei/ dateien gespeichert werden) 
		String extractpfad = pfad + foldername;

		Filetools.deletedir(new File(extractpfad));
		
        if(Filetools.error == true){
			JOptionPane.showMessageDialog(null,"Datei/en Konten Nicht Vollständig gelöscht werden! (Programm noch am laufen ?)"
					,"Error!", JOptionPane.ERROR_MESSAGE);
			Filetools.error = false;
        } else {
    		System.out.println("Deleted!");
        }
	}
	
}
