package module;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

public class Filetools {
	public static boolean error = false;
	public static void deletefile(String dateipfad){
		try{

			File file = new File(dateipfad);
			if (file.exists() == true){
			if(file.delete()){
				System.out.println(file.getName() + " is deleted!");
			}else{
				System.out.println("Delete operation is failed.");
				JOptionPane.showMessageDialog(null,"Datei/en Konten Nicht gel�scht werden! (Programm noch am laufen ?)"
						,"Error!", JOptionPane.ERROR_MESSAGE);
			}
			}else{
				System.out.println("Datei/en Sind bereits gel�scht!");
				JOptionPane.showMessageDialog(null,"Datei/en Sind bereits gel�scht!"
						,"Error!", JOptionPane.INFORMATION_MESSAGE);
			}	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void deletedir(File ordnerpfad) {
		try{	
		if (ordnerpfad.exists() == true){
		    File[] contents = ordnerpfad.listFiles();
		    if (contents != null) {
		        for (File f : contents) {
		            deletedir(f);
		        }
		    }

		    if (ordnerpfad.delete() == true){
				System.out.println(ordnerpfad + " is deleted!");
			}else{
				error = true;
				System.out.println("Delete operation is failed.");
			}
		}else {
			System.out.println("Datei/en Sind bereits gel�scht!");
			JOptionPane.showMessageDialog(null,"Datei/en Sind bereits gel�scht!"
					,"Error!", JOptionPane.INFORMATION_MESSAGE);
		}	
	}catch(Exception e){
		e.printStackTrace();
	}
	}
	public static void deletedirsilent(File ordnerpfad) {
		try{	
		if (ordnerpfad.exists() == true){
		    File[] contents = ordnerpfad.listFiles();
		    if (contents != null) {
		        for (File f : contents) {
		            deletedir(f);
		        }
		    }

		    if (ordnerpfad.delete() == true){
				System.out.println(ordnerpfad + " is deleted!");
			}else{
				error = true;
				System.out.println("Delete operation is failed.");
			}
		}else {
			System.out.println("Datei/en Sind bereits gel�scht!");
		}	
	}catch(Exception e){
		e.printStackTrace();
	}
	}
	
	public static void copyFile(String from, String to) {
        copyFile(from, to, Boolean.FALSE);
    }

    public static void copyFile(String from, String to, Boolean overwrite) {
        try {
            File fromFile = new File(from);
            File toFile = new File(to);

            if (!fromFile.exists()) {
                throw new IOException("File not found: " + from);
            }
            if (!fromFile.isFile()) {
                throw new IOException("Can't copy directories: " + from);
            }
            if (!fromFile.canRead()) {
                throw new IOException("Can't read file: " + from);
            }

            if (toFile.isDirectory()) {
                toFile = new File(toFile, fromFile.getName());
            }

            if (toFile.exists() && !overwrite) {
                throw new IOException("File already exists.");
            } else {
                String parent = toFile.getParent();
                if (parent == null) {
                    parent = System.getProperty("user.dir");
                }
                File dir = new File(parent);
                if (!dir.exists()) {
                    throw new IOException("Destination directory does not exist: " + parent);
                }
                if (dir.isFile()) {
                    throw new IOException("Destination is not a valid directory: " + parent);
                }
                if (!dir.canWrite()) {
                    throw new IOException("Can't write on destination: " + parent);
                }
            }

            FileInputStream fis = null;
            FileOutputStream fos = null;
            try {

                fis = new FileInputStream(fromFile);
                fos = new FileOutputStream(toFile);
                byte[] buffer = new byte[4096];
                int bytesRead;

                while ((bytesRead = fis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }

            } finally {
                if (from != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                      System.out.println(e);
                    }
                }
                if (to != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Problems when copying file.");
        }
    }
	
}
