package userinterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;





class ContextMenuMainGUI extends JPopupMenu {
	private static final long serialVersionUID = 1L;
	
    public ContextMenuMainGUI(){
        
    	
    	JMenuItem anItem = new JMenuItem("Metal");
    	add(anItem);
    	JMenuItem anItem1 = new JMenuItem("Nimbus");
    	add(anItem1);
    	JMenuItem anItem2 = new JMenuItem("SystemLook");
    	add(anItem2);
    	JMenuItem anItem3 = new JMenuItem("Graphite");
    	add(anItem3);
    	JMenuItem anItem4 = new JMenuItem("Fast");
    	add(anItem4);
    	JMenuItem anItem5 = new JMenuItem("HiFi");
    	add(anItem5);
    	JMenuItem anItem6 = new JMenuItem("Aero");
    	add(anItem6);
    	JMenuItem anItem7 = new JMenuItem("McWin");
    	add(anItem7);
    	JMenuItem anItem8 = new JMenuItem("Texture");
    	add(anItem8);
        
    	// Action Listener
    	anItem.addActionListener(menuActionListener);
    	anItem1.addActionListener(menuActionListener);
    	anItem2.addActionListener(menuActionListener);
    	anItem3.addActionListener(menuActionListener);
    	anItem4.addActionListener(menuActionListener);
    	anItem5.addActionListener(menuActionListener);
    	anItem6.addActionListener(menuActionListener);
    	anItem7.addActionListener(menuActionListener);
    	anItem8.addActionListener(menuActionListener);
    	
    }
	ActionListener menuActionListener = new ActionListener(){

	    @Override
	    public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand() == "Metal") {	
				System.out.println("theme Metal");
				TheMainGui.setLAF(1);
			}
			else if (e.getActionCommand() == "Nimbus") {		 
				System.out.println("theme Nimbus");
				TheMainGui.setLAF(2);
			}
			else if (e.getActionCommand() == "SystemLook") {		 
				System.out.println("theme SystemLook");
				TheMainGui.setLAF(3);
			}
			else if (e.getActionCommand() == "Graphite") {		 
				System.out.println("theme Graphite");
				TheMainGui.setLAF(4);
			}
			else if (e.getActionCommand() == "Fast") {		 
				System.out.println("theme Fast");
				TheMainGui.setLAF(5);
			}
			else if (e.getActionCommand() == "HiFi") {		 
				System.out.println("theme HiFi");
				TheMainGui.setLAF(6);
			}
			else if (e.getActionCommand() == "Aero") {		 
				System.out.println("theme Aero");
				TheMainGui.setLAF(7);
			}
			else if (e.getActionCommand() == "McWin") {		 
				System.out.println("theme McWin");
				TheMainGui.setLAF(8);
			}
			else if (e.getActionCommand() == "Texture") {		 
				System.out.println("theme Texture");
				TheMainGui.setLAF(9);
			}
	    }
	    
	};
}

class PopClickListener extends MouseAdapter {
    public void mousePressed(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    public void mouseReleased(MouseEvent e){
        if (e.isPopupTrigger())
            doPop(e);
    }

    private void doPop(MouseEvent e){
    	ContextMenuMainGUI menu = new ContextMenuMainGUI();
        menu.show(e.getComponent(), e.getX(), e.getY());
    }
}
