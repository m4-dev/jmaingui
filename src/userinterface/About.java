package userinterface;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.TitledBorder;

public class About extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Create the dialog.
	 */
	public About() {
		setSize(350, 212);
		setResizable(false);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "About", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(192, 192, 192)));
		panel.setBounds(0, 0, 334, 173);
		getContentPane().add(panel);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setBounds(12, 21, 140, 140);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Desktop.isDesktopSupported()){
				    Desktop desktop = Desktop.getDesktop();
				    try {
				        desktop.browse(new URI(module.Variablen.myurl));
				    } catch (IOException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    } catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
				    Runtime runtime = Runtime.getRuntime();
				    try {
				        runtime.exec("xdg-open " + module.Variablen.myurl);
				    } catch (IOException e) {
				        // TODO Auto-generated catch block
				        e.printStackTrace();
				    }
				}
			}
		});
		panel.setLayout(null);
		btnNewButton.setIcon(new ImageIcon(getClass().getResource("/resource/icon/MArcellxDGlow.png")));
		panel.add(btnNewButton);
		
		JTextPane txtpnKontaktMrcellxdgmailcompgp = new JTextPane();
		txtpnKontaktMrcellxdgmailcompgp.setBounds(165, 21, 159, 140);
		panel.add(txtpnKontaktMrcellxdgmailcompgp);
		txtpnKontaktMrcellxdgmailcompgp.setPreferredSize(new Dimension(1, 7));
		txtpnKontaktMrcellxdgmailcompgp.setCaretPosition(0);
		txtpnKontaktMrcellxdgmailcompgp.setEditable(false);
		txtpnKontaktMrcellxdgmailcompgp.setText("Kontakt---\r\nM4rcellxD@gmail.com \r\n{PGP Only}\r\n\r\nAboutMe---\r\nName: M4rcellxD \r\nJob: IT Specialist\r\nSite: M4-Dev.net\r\n\r\n\r\nCredits---\r\nini4j - lib\r\nJTattoo - Theme\r\nURLPlayer.class - MP3 Player");
	}
}
