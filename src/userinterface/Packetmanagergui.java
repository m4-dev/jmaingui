package userinterface;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import module.Bypass;
import module.packet.Packetdeleter;
import module.packet.Packetinstaller;
import module.packet.Packetstarter;

public class Packetmanagergui extends JFrame {
	/**
	 * 
	 */
	
	String Treeelement;
	private static final long serialVersionUID = -7792876904838833366L;
	private JPanel contentPane;


	/**
	 * Create the frame.
	 */
	
	public Packetmanagergui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setLocation(null);
		setSize(370, 220);
		setResizable(false);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 218, 168);
		contentPane.add(scrollPane);
		JTree tree = new JTree();
		
		JButton btnDownload = new JButton("Download");


		
		
		
		
		
			tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Packet Manager") {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				{
					DefaultMutableTreeNode rootnode;
					DefaultMutableTreeNode zweiternode;

					// M4-dev - Node
					rootnode = new DefaultMutableTreeNode("M4-dev");
					// Unterpunkt von M4-dev
					zweiternode = new DefaultMutableTreeNode("SystemTools");
					// Unterpunkt von SystemTools
					zweiternode.add(new DefaultMutableTreeNode("System Console"));
					// F�ge SystemTools Node zu M4-dev hinzu
					rootnode.add(zweiternode);

					// GameTools - Node
					zweiternode = new DefaultMutableTreeNode("GameTools");
					// Unterpunkt von GameTools
					zweiternode.add(new DefaultMutableTreeNode("Anti-AFK"));
					// F�ge GameTools Node zu M4-dev hinzu
					rootnode.add(zweiternode);
					
					
					// F�ge M4-dev Node zu treeview hinzu ( add(rootnode); )
					add(rootnode);
					
					// other - Node
					rootnode = new DefaultMutableTreeNode("other");
					// Unterpunkt von other
					rootnode.add(new DefaultMutableTreeNode("4K Video Downloader"));
					
					// F�ge M4-dev Node zu treeview hinzu ( add(rootnode); )
					add(rootnode);
					
					// tweaks - Node
					rootnode = new DefaultMutableTreeNode("tweaks");
					// Unterpunkt von tweaks
					rootnode.add(new DefaultMutableTreeNode("EingabeaufforderungEin"));
					rootnode.add(new DefaultMutableTreeNode("EingabeaufforderungAus"));
					// F�ge M4-dev Node zu treeview hinzu ( add(rootnode); )
					add(rootnode);

					
				}
			}
		));
		JLabel lblToolname = new JLabel("");
		lblToolname.setFont(new Font("Dialog", Font.BOLD, 10));
		lblToolname.setBounds(240, 93, 114, 16);
		contentPane.add(lblToolname);
		
		 MouseListener ml = new MouseAdapter() {
		     public void mousePressed(MouseEvent e) {
		         int selRow = tree.getRowForLocation(e.getX(), e.getY());
		         if(selRow != -1) {
		             if(e.getClickCount() == 1) {           
		                 DefaultMutableTreeNode selectedElement 
		                 =(DefaultMutableTreeNode)tree.getSelectionPath().getLastPathComponent();	
		                 Treeelement = selectedElement.getUserObject().toString();
		                 
		                 lblToolnasme(Treeelement);
		                 if (Treeelement.contains("Eingabeaufforderung")){
		                	 btnDownload.setText("Start");
		                 }else{
		                	 btnDownload.setText("Download");
		                 }

		             }
		         }
		     }

				private void lblToolnasme(String treeelement) {
					lblToolname.setText(treeelement);
				}



		 };
		 tree.addMouseListener(ml);		
		
	     tree.getSelectionModel().setSelectionMode
	                (TreeSelectionModel.SINGLE_TREE_SELECTION);

		tree.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		scrollPane.setViewportView(tree);
		


		
		JButton btnBackToMaingui = new JButton("Back");
		btnBackToMaingui.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TheMainGui.openthegui();
				setVisible(false);
				dispose();
			}
		});
		btnBackToMaingui.setBounds(283, 12, 71, 23);
		contentPane.add(btnBackToMaingui);
		
		
		
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				downloadbuttonaufgabe(Treeelement);
			}
		});
		btnDownload.setBounds(240, 117, 114, 26);
		contentPane.add(btnDownload);
		
		JButton btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deletebuttonaufgabe(Treeelement);
			}
		});
		btnDelete.setBounds(240, 154, 114, 26);
		contentPane.add(btnDelete);
		
		JButton btnUacBypass = new JButton("UAC Bypass");
		btnUacBypass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();

				int choice = chooser.showOpenDialog(null);

				if (choice != JFileChooser.APPROVE_OPTION) return;

				File chosenFile = chooser.getSelectedFile();
				
				Bypass.TheBypass(chosenFile.toString());
			}
		});
		btnUacBypass.setBounds(240, 56, 114, 26);
		contentPane.add(btnUacBypass);
		btnUacBypass.setEnabled(false);
		
	}
	
	public void downloadbuttonaufgabe(String test){
		switch (test){
        		default:                 
        			JOptionPane.showMessageDialog(null,"Nothing To Download!",
        						"Error", JOptionPane.INFORMATION_MESSAGE);
        			break;
		        case "Anti-AFK":    
		        	Packetinstaller.AntiAFK();
		            break;
		        case "4K Video Downloader":    
		        	Packetinstaller.fourKDownloader();
		            break;
		        case "System Console":    
		        	Packetinstaller.systemconsole();
		            break;
		        case "EingabeaufforderungEin":    
		        	Packetstarter.contexteingabeEin();;
		            break;
		        case "EingabeaufforderungAus":    
		        	Packetstarter.contexteingabeAus();;
		            break;

		}
	}
	public void deletebuttonaufgabe(String test){
		switch (test){
				default:                 
					JOptionPane.showMessageDialog(null,"Nothing To Download! ",
								"Error", JOptionPane.INFORMATION_MESSAGE);
					break;
		        case "Anti-AFK":    
		        	Packetdeleter.AntiAFKDelete();
		            break;
		        case "4K Video Downloader":    
		        	Packetdeleter.fourKDownloaderDelete();
		            break;
		        case "System Console":    
		        	Packetdeleter.systemconsoleDelete();
		            break;
		}
	}
}
