package userinterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import module.Saveuserwindowpos;
import module.mp3player.MP3Player;
import module.packet.Packetstarter;


public class TheMainGui extends JFrame {
	static boolean streamis = false;
	boolean playerisopen = false;
	static boolean volmin = false;
	static boolean volmax = false;
	static Wini ini;
	static String internetradiostream = module.Variablen.iloveradio;
	
	
	public static boolean setvolmin(){
		volmax = false;
		return volmin = true;	
	}
	public static boolean setvolmax(){
		volmin = false;
		return volmax = true;	
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 536773303018785468L;
	private JPanel contentPane;

	 public static int lAF; 
	 public static void updateLAF() { 
	        try { 
	        	if (new File(module.Variablen.settingsinipath).exists()){
		  		  	ini = new Wini(new File(module.Variablen.settingsinipath));

				 	lAF = Integer.parseInt(ini.get("main", "lAF"));
				 	System.out.println("Your default Theme is: " + lAF);
	        	}
	  		  	
	            String plaf = ""; 
	            if (lAF == 1) { 
	                plaf = "javax.swing.plaf.metal.MetalLookAndFeel"; 
	            } else if (lAF == 2) { 
	                plaf = "javax.swing.plaf.nimbus.NimbusLookAndFeel"; 
	            } else if (lAF == 3) { 
	                plaf = UIManager.getSystemLookAndFeelClassName(); 
	            } else if (lAF == 4) { 
	                plaf = "com.jtattoo.plaf.graphite.GraphiteLookAndFeel"; 
	            } else if (lAF == 5) { 
	                plaf = "com.jtattoo.plaf.fast.FastLookAndFeel"; 
	            } else if (lAF == 6) { 
	                plaf = "com.jtattoo.plaf.hifi.HiFiLookAndFeel"; 
	            } else if (lAF == 7) { 
	                plaf = "com.jtattoo.plaf.aero.AeroLookAndFeel"; 
	            } else if (lAF == 8) { 
	                plaf = "com.jtattoo.plaf.mcwin.McWinLookAndFeel"; 
	            } else if (lAF == 9) { 
	                plaf = "com.jtattoo.plaf.texture.TextureLookAndFeel"; 
	            }
	            UIManager.setLookAndFeel(plaf); 
	            //SwingUtilities.updateComponentTreeUI(thegui);
	            
	        } catch (UnsupportedLookAndFeelException ue) { 
	            System.err.println(ue.toString()); 
	        } catch (ClassNotFoundException ce) { 
	            System.err.println(ce.toString()); 
	        } catch (InstantiationException ie) { 
	            System.err.println(ie.toString()); 
	        } catch (IllegalAccessException iae) { 
	            System.err.println(iae.toString()); 
	        } catch (InvalidFileFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	    } 
	
	 public static void setLAF(int zahl){
		 lAF = zahl;
		 try {
			 String path = module.Variablen.settingsinipath;
			// Use relative path for Unix systems
			File f = new File(path);

			f.getParentFile().mkdirs(); 
			f.createNewFile();
			 
			
			ini = new Wini(new File(module.Variablen.settingsinipath));
			ini.put("main", "lAF", lAF);
			ini.store();
		  
		  
		 } catch (InvalidFileFormatException e) {
		  System.out.println("Invalid file format.");
		 } catch (IOException e) {
		  System.out.println("Problem reading file.");
		 }
		 Runtime runtime = Runtime.getRuntime();
		 try {
			@SuppressWarnings("unused")
			Process proc = runtime.exec("java -jar " + module.Variablen.jarpathstring);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 System.exit(0);
	 }

	
	
	public static void openthegui(){
		
		// Load THeme
		updateLAF();
		
		// Load internetradio
		if (new File(module.Variablen.settingsinipath).exists()){
		  	try {
				ini = new Wini(new File(module.Variablen.settingsinipath));
			} catch (InvalidFileFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		internetradiostream = ini.get("musik", "internetradio");
	 	System.out.println("Your default InternetRadio: " + internetradiostream);
		}
		
		
		TheMainGui thegui = new TheMainGui();
		thegui.addMouseListener(new PopClickListener());
		thegui.addComponentListener(new ComponentAdapter() {
		    public void componentMoved(ComponentEvent e) {
		       // updateText(thegui);
		        try {
					Saveuserwindowpos.storeOptions(thegui);
					Thread.sleep(10);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        
		    }
		});
		Saveuserwindowpos.storeOptionspath();
        File optionsFile = new File(Saveuserwindowpos.fileName);
        if (optionsFile.exists()) {
            try {
                Saveuserwindowpos.restoreOptions(thegui);
            } catch(IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
        	thegui.setLocationByPlatform(true);
        }
        thegui.setVisible(true);
	}
	
	
	
	
   /** FOR DEBUG
	private static void updateText( final JFrame jf) {
        System.out.println("JFrame is located at: " + jf.getLocation());
    }
    */
	
	
	/**
	 * Create the frame.
	 */

	public TheMainGui() {
		setResizable(false);
		setTitle("jMainGUI " + "[" + module.Variablen.MyVersion + "]");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(345, 188);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		setLocationRelativeTo(null);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "by M4rcellxD", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(192, 192, 192)));
		panel.setBounds(0, 0, 337, 158);
		contentPane.add(panel);
		panel.setLayout(null);
		//setBounds(100, 100, 345, 188); Closed player
		//pack();
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setMaximum(10);

		
		
		
		
		JButton btnOpen = new JButton("");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (playerisopen == false){
					setSize(345, 230);
					playerisopen = true;
					btnOpen.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/close.png")));
				} else if (playerisopen == true){
					setSize(345, 188);
					playerisopen = false;
					btnOpen.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/open.png")));
				}
				
				
			}
		});
		btnOpen.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/open.png")));
		btnOpen.setBounds(81, 85, 25, 26);
		panel.add(btnOpen);
		progressBar.setBounds(14, 61, 159, 14);
		panel.add(progressBar);
		//progressBar.setValue(100);
		
		JLabel lblMaingui = new JLabel("jMainGUI");
		lblMaingui.setBounds(14, 18, 159, 39);
		panel.add(lblMaingui);
		lblMaingui.setFont(new Font("Dialog", Font.BOLD, 36));
		
		
    	
		// JList 
		JList<Object> list = new JList<Object>();
		list.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		
		// SET ITEM 
		list.setModel(new javax.swing.AbstractListModel<Object>() {
			private static final long serialVersionUID = 1L;
			String[] strings = {"",""};
		    public int getSize() { return strings.length; }
		    public Object getElementAt(int i) { return strings[i]; }
		});
		// SET ITEM DYNAMIK
		DefaultListModel<Object> listModel = new DefaultListModel<Object>();
		list.setModel(listModel);
		
		/** TODO CHECK IF INSTALLED */

		if (module.packet.Packetconverter.link2existfile(module.Variablen.puttylink, module.Variablen.pfadwin)) {
			listModel.addElement("Anti-AFK");
		} 
		if (module.packet.Packetconverter.link2existfile(module.Variablen.fourkdownloaderlink, module.Variablen.pfadwin)) {
			listModel.addElement("4K Video Downloader");
		}
		if (module.packet.Packetconverter.link2existfile(module.Variablen.systemconsolelink, module.Variablen.pfadwin)) {
			listModel.addElement("System Console");
		}
		
		// MOUSE LISTENER FOR DOUBLECLICK DETECTION
		list.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        JList<?> list = (JList<?>)evt.getSource();
		        if (evt.getClickCount() == 2) {
		        	System.out.println();
		            // Double-click detected
		        	if (list.getSelectedValue() != null) {
		        		start((String)list.getSelectedValue());
		        	}
		        	
		        	
		        	
		        }
		    }
		});

		
		list.setBounds(185, 18, 138, 131);
		panel.add(list);
		//-------------------------------------
		
		JButton btnPacketManager = new JButton("Packet Manager");
		btnPacketManager.setBounds(14, 123, 159, 26);
		panel.add(btnPacketManager);
		

		JButton btnppp = new JButton("...");
		btnppp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
                JPanel panel = new JPanel();
                panel.add(new JLabel("Radiosender:"));
                DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
                model.addElement("Technobase");
                model.addElement("ILoveRadio");
                model.addElement("ILoveRadio-Bass");
                model.addElement("FantasyDanceFM");
                model.addElement("BigCityBeats");
                model.addElement("M4-dev-Stream");
                JComboBox<String> comboBox = new JComboBox<String>(model);
                panel.add(comboBox);

                int result = JOptionPane.showConfirmDialog(contentPane, panel, "Treffe eine wahl", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                switch (result) {
                    case JOptionPane.OK_OPTION:
                        if (MP3Player.get().isPlaying()) {
                        	System.out.println("not working wile playing");
                        	JOptionPane.showMessageDialog(null,"Beende den radio stream und probiere es erneut!","Sorry :(", JOptionPane.INFORMATION_MESSAGE);
                        } else {
                        	setmp3url(comboBox.getSelectedItem().toString());
                        }
                        System.out.println("You selected " + comboBox.getSelectedItem());
                        break;
                }
			}
		});
		btnppp.setBounds(120, 169, 25, 24);
		contentPane.add(btnppp);

		JButton btnLauter = new JButton("Lauter");
		
		JButton btnLeiser = new JButton("Leiser");
		
		
		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				if (streamis == false){
					btnPlay.setEnabled(false);
						MP3Player.get().play(internetradiostream);
					
					setTitle("jMainGUI " + "[" + module.Variablen.MyVersion + "]" + " [Playing Stream]");
					
					progressBar.setValue(10);
					btnPlay.setText("Pause");
					btnPlay.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/Pause.png")));
					
					streamis = true;
					btnLauter.setEnabled(true);
					btnLeiser.setEnabled(true);
					btnPlay.setEnabled(true);
					btnppp.setEnabled(false);
				} else if (streamis == true){
					btnPlay.setEnabled(false);
					MP3Player.get().stop();
					setTitle("jMainGUI " + "[" + module.Variablen.MyVersion + "]");
					progressBar.setValue(0);
					btnPlay.setText("Play");
					btnPlay.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/Play.png")));
					
					streamis = false;
					btnLauter.setEnabled(false);
					btnLeiser.setEnabled(false);
					btnPlay.setEnabled(true);
					btnppp.setEnabled(true);
				}
				Thread.sleep(100);
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnPlay.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/Play.png")));
		btnPlay.setBounds(10, 170, 99, 23);
		contentPane.add(btnPlay);

		

		btnLauter.setEnabled(false);
		btnLeiser.setEnabled(false);
		btnLeiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					MP3Player.get().volumeDown();
				} catch(Exception e2) {
					JOptionPane.showMessageDialog(null,"OPENJDK NOT SUPPORTED Error: " + e2,"Sorry :(", JOptionPane.INFORMATION_MESSAGE);
					System.out.println("not working sry " + e2);
				}
				
				volmax = false;
				if (volmin == true){
					btnLeiser.setEnabled(false);
				}
				if (volmax == false){
					btnLauter.setEnabled(true);
				}
			}
		});
		btnLeiser.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/leiser.png")));
		btnLeiser.setBounds(249, 169, 82, 23);
		contentPane.add(btnLeiser);
		
		
		btnLauter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					MP3Player.get().volumeUp();
				} catch(Exception e2) {
					JOptionPane.showMessageDialog(null,"OPENJDK NOT SUPPORTED Error: " + e2,"Sorry :(", JOptionPane.INFORMATION_MESSAGE);
					System.out.println("not working sry " + e2);
				}
				volmin = false;
				if (volmax == true){
					btnLauter.setEnabled(false);
				}
				if (volmin == false){
					btnLeiser.setEnabled(true);
				}
			}
		});
		btnLauter.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/lauter.png")));
		btnLauter.setBounds(156, 169, 82, 23);
		contentPane.add(btnLauter);
		

        if (MP3Player.get().isPlaying()) {
			setTitle("jMainGUI " + "[" + module.Variablen.MyVersion + "]" + " [Playing Stream]");
			
			progressBar.setValue(10);
			btnPlay.setText("Pause");
			btnPlay.setIcon(new ImageIcon(TheMainGui.class.getResource("/resource/icon/Pause.png")));
			
			streamis = true;
			btnLauter.setEnabled(true);
			btnLeiser.setEnabled(true);
			btnppp.setEnabled(false);
        	
        }
        
		
		JButton btnAbout = new JButton("About");
		btnAbout.setBounds(14, 85, 67, 26);
		panel.add(btnAbout);
		
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				About dialog = new About();
		        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		        dialog.setVisible(true);
			}
		});
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(106, 85, 67, 26);
		panel.add(btnExit);
		

		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	                System.exit(0);
			}
		});
		btnPacketManager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Packetmanagergui pm = new Packetmanagergui();
				pm.addMouseListener(new PopClickListener());
				pm.addComponentListener(new ComponentAdapter() {
				    public void componentMoved(ComponentEvent e) {
				       // updateText(thegui);
				        try {
							Saveuserwindowpos.storeOptions(pm);
							Thread.sleep(10);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				        
				    }
				});
				Saveuserwindowpos.storeOptionspath();
		        File optionsFile = new File(Saveuserwindowpos.fileName);
		        if (optionsFile.exists()) {
		            try {
		                Saveuserwindowpos.restoreOptions(pm);
		            } catch(IOException ioe) {
		                ioe.printStackTrace();
		            }
		        } else {
		        	pm.setLocationByPlatform(true);
		        }
		        pm.setVisible(true);
				setVisible(false);
				dispose();
			}
		});
	}
	
	/** TODO start */
	public void start(String test){
		switch (test){
        		default:                 
        			JOptionPane.showMessageDialog(null,"Nothing To Start!",
        						"Error", JOptionPane.INFORMATION_MESSAGE);
        			break;
		        case "Anti-AFK":    
		        	Packetstarter.AntiAFK();
		            break;
		        case "4K Video Downloader":    
		        	Packetstarter.fourKDownloader();
		            break;
		        case "System Console":    
		        	Packetstarter.systemconsole();
		            break;

		}
	}
	public void setmp3urltoini(String test){
		try {
		String path = module.Variablen.settingsinipath;
		// Use relative path for Unix systems
		File f = new File(path);

		f.getParentFile().mkdirs(); 
		f.createNewFile();

		ini = new Wini(new File(module.Variablen.settingsinipath));
		ini.put("musik", "internetradio", test);
		ini.store();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void setmp3url(String test){
		switch (test){
        		default:                 
        			JOptionPane.showMessageDialog(null,"ERROR ! mp3",
        						"Error", JOptionPane.INFORMATION_MESSAGE);
        			break;
		        case "Technobase":    
		        	setmp3urltoini(module.Variablen.technobase);
		        	internetradiostream = module.Variablen.technobase;
		            break;
		        case "ILoveRadio":    
		        	setmp3urltoini(module.Variablen.iloveradio);
		        	internetradiostream = module.Variablen.iloveradio;
		            break;
		        case "ILoveRadio-Bass":    
		        	setmp3urltoini(module.Variablen.iloveradiobass);
		        	internetradiostream = module.Variablen.iloveradiobass;
		            break;
		        case "M4-dev-Stream":    
		        	setmp3urltoini(module.Variablen.iloveradiobass);
		        	internetradiostream = module.Variablen.iloveradiobass;
		            break;
		        case "FantasyDanceFM":    
		        	setmp3urltoini(module.Variablen.fantasydancefm);
		        	internetradiostream = module.Variablen.fantasydancefm;
		            break;
		        case "BigCityBeats":    
		        	setmp3urltoini(module.Variablen.bigcitybeats);
		        	internetradiostream = module.Variablen.bigcitybeats;
		            break;
		}
	}
}
